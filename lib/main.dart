import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.orange,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String img =
        'https://images.tokopedia.net/img/cache/700/VqbcmM/2021/2/2/4cba7c1b-9344-40aa-971f-4353e2cc142b.jpg';

    var column = Column(
      children: [
        Container(
          height: 200,
          width: (MediaQuery.of(context).size.width / 2) - 40,
          child: Image.network(img),
        ),
        Container(
          padding: EdgeInsets.all(6),
          width: (MediaQuery.of(context).size.width / 2) - 40,
          decoration: const BoxDecoration(
            color: Colors.orange,
            borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(12),
              bottomRight: Radius.circular(12),
            ),
          ),
          child: Column(
            // ignore: prefer_const_literals_to_create_immutables
            children: [
              const Text('Short Mocchaciato'),
              const Text(
                'IDR 49.9999',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                ),
              ),
            ],
          ),
        ),
      ],
    );

    return Scaffold(
      body: ListView(
        children: [
          Container(
            padding: const EdgeInsets.all(24),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              // ignore: prefer_const_literals_to_create_immutables
              children: [
                const Icon(Icons.menu),
                const Icon(Icons.access_alarm),
              ],
            ),
          ),
          // search bar
          Container(
            margin: const EdgeInsets.symmetric(
              horizontal: 24,
            ),
            height: 70,
            decoration: BoxDecoration(
              color: Colors.brown.shade50,
              borderRadius: const BorderRadius.all(
                Radius.circular(12),
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(24.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                // ignore: prefer_const_literals_to_create_immutables
                children: [
                  const Text('Search'),
                  const Icon(Icons.search),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 24, top: 24, bottom: 12),
            child: Row(
              // ignore: prefer_const_literals_to_create_immutables
              children: [
                const Text(
                  'Coffee',
                  style: TextStyle(
                    color: Colors.orange,
                    fontSize: 18,
                  ),
                ),
                // ignore: prefer_const_constructors
                Padding(
                  padding: const EdgeInsets.only(left: 24),
                  child: const Text(
                    'Coffee',
                    style: TextStyle(
                      color: Colors.orangeAccent,
                      fontSize: 18,
                    ),
                  ),
                ),
              ],
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(
              left: 24,
            ),
            child: Text(
              'Popular Drink',
              style: TextStyle(
                fontSize: 40,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12),
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  column,
                  column,
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12),
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  column,
                  column,
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
